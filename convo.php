<?php

class Convo {
    private $user = "";
    public $id = -1;
    
    public function __construct($user, $id) {
        $this->user = $user;
        $this->id = $id;
    }
    
    public function getMessages() {
        global $db;
        $r = $db->query("SELECT * FROM `messages` WHERE convo=$this->id AND user='$user'");
        $messages = array();
	while ($row = $db->iter($r)) {
            $messages[] = new ConvoMessage($this->user, $row);
        }
        return $messages;
    }
}

class ConvoMessage {
    private $user = "";
    public $id = -1;
    public $subject, $to, $cc, $from, $from_short, $replyto;
    public $body;
    public $extid;
    
    public function __construct($user, $row) {
        global $db;
        $this->user = $user;
        $this->row = $row;
        $this->id = $row['id'];
        $this->body = $this->nice_plain($row['body']);
        $this->date = $row['created'];
        $this->udate = strtotime($row['created']);
        
        $s = "SELECT * FROM `headers` WHERE `message`=$this->id AND `user`=$this->user ";
        $this->subject = $db->get_list($s."AND `name`='Subject'");
        $this->to = $db->get_list($s."AND name='To'");
        $this->cc = $db->get_list($s."AND name='CC'");
        $this->from = $db->get_list($s."AND name='From'");
        preg_match("/^(.*?)(<(.*)>)?$/", $this->from, $m);
        $this->from_short = $m[1] or $m[3];
        
        $this->replyto = $this->from; # FIXME
        $this->extid = $db->get_list($s."AND name='Message-ID'");
    }
    
    public function nice_plain($text) {
	$split = preg_split("!([/\w\-\.\?\&\=\#]{3,}:/{2}[\w\.]{2,}[/\w\-\.\?\&\=\#]*)!e", $text, -1, PREG_SPLIT_DELIM_CAPTURE);
	$out = "";
	foreach ($split as $key => $bit) {
		if ($key%2) {
			$out .= "<a href=\"$bit\">$bit</a>";
		} else {
			$out .= htmlspecialchars($bit);
		}
	}
	return nl2br($out);
    }
    
    public function nice_re($sub) {
	if (ereg('Re: .*',$sub))
		return $sub;
	else return "Re: ".$sub;
    }
    
    public function indented() {
        $mess = "";
        $mess .= "\n\nOn ".date("j F Y H:i",$this->udate).", ".$this->from." wrote:\n";
	if (get_setting("html") == "false") {
		global $mode;
		if ($mode == "HTML") $mess = strip_tags(preg_replace('/\<br(\s*)\/?\>/i', "\n"));
		$mess .= ">" . ereg_replace("\n","\n> ", $this->row["body"]);
		return $mess;
	} else {
            return "<blockquote>".$this->body."</blockquote>";
        }
    }
}

?>