
<?php $me = "index.php"; ?>
<div id="intro">Welcome <?php echo $username ?>, it is <?php echo date("H:i") ?> <a href="<?php echo $me ?>?do=logout">Logout</a>?</div>

<div id="sidebar">
<a href="<?php echo $me ?>?do=new">New Email</a>
<h2>Folders</h2>
<a href="<?php echo $me ?>?do=list&amp;pos=0&amp;view=inbox">Inbox</a><br/>
<a href="<?php echo $me ?>?do=list&amp;pos=0&amp;view=arc">All Mail</a><br/>
<a href="<?php echo $me ?>?do=list&amp;pos=0&amp;view=star">Starred</a><br/>
<a href="<?php echo $me ?>?do=list&amp;pos=0&amp;view=sent">Sent</a><br/>
<a href="<?php echo $me ?>?do=list&amp;pos=0&amp;view=bin">Bin</a><br/>
<br/>
<a href="<?php echo $me ?>?do=contacts">Contacts</a><br/>
<br/>
<a href="<?php echo $me ?>?do=settings">Settings</a><br/>
<br/>
<h2>Tags</h2>
<?php
if (isset($tags)) {
    foreach ($tags as $tag) { ?>
        <a href="?do=list&amp;pos=0&amp;view=tag&amp;name="<?php $tag ?>"><?php $tag ?></a><br/>
    <?php }
}
?>

</div>

<div id="main">
    <?php contents(); ?>
</div>
