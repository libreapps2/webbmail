<?php

class Message {
    public $id = -1;
    private $user;
    private $username;
    private $headers = array();
    private $head;
    private $body;
    public $TYPE_NORMAL = 0;
    public $TYPE_SENT = 1;
    public $TYPE_DRAFT = 2;
    public $type = 0;
    
    public function __construct($username) {
        global $db;
        $this->username = $db->escape($username);
        if ($r = $db->get_first("SELECT id FROM `users` WHERE username='$this->username'")) {
            $this->user = $r["id"];
        }
        else exit;
    }
    
    public function finish($convo=FALSE) {
        global $db;
        if ($this->id >= 0) {
            $db->query("UPDATE `messages` SET `head`='$this->head', `body`='$this->body', `user`='$this->user', `type`='$this->type', `created`=NOW() WHERE `id`=$this->id");
        }
        else {
            $this->id = $db->query("INSERT INTO `messages` (`head`, `body`, `user`, `type`, `created`) " .
                "VALUES ('$this->head', '$this->body', '$this->user', '$this->type', NOW())", true);
        }
        
        ## Assign message to a conversation
        foreach ($this->headers as $header) {
            $this->real_add_header($header[0], $header[1]);
            if ($convo == FALSE && $header[0] == "References") {
                preg_match_all("/<(.*?)@(.*?)>/", $header[1], $m);
                for ($i=0; $i<sizeof($m[0]); $i++) {
                    if ($m[2][$i] == preg_replace("/^.*?\@/", "", $this->username)) {
                        $row = $db->get_first("SELECT `convo` FROM `messages` WHERE id='".$db->escape($m[1][$i])."'");
                        if ($row) {
                            $convo = $row["convo"];
                        }
                    }
                }
            }
        }
        if ($convo === FALSE) {
            $convo = $db->query("INSERT INTO `convos` (`created`, `updated`, `user`) VALUES (NOW(), NOW(), '$this->user')", true);
        }
    $db->query("UPDATE `messages` SET `convo`='$convo' WHERE `id`=$this->id");
        $this->update_convo($convo);
    }
    
    public function update_convo($id) {
        global $db;
        $row = $db->get_first("SELECT `messages`.`id`, `headers`.`value`,`messages`.`body` FROM `messages` JOIN `headers` ON `headers`.`message`=`messages`.`id` WHERE `headers`.`name`='Subject' AND `messages`.`convo`=$id ORDER BY `messages`.`read` ASC, `messages`.`created` ASC");
        $summary = $db->escape(substr($row["value"]." - " . strip_tags($row["body"]), 0, 50));
        $participants = $db->escape($db->get_list("SELECT `messages`.`id`, `headers`.`value` FROM `messages` JOIN `headers` ON `headers`.`message`=`messages`.`id` WHERE `headers`.`name`='From' AND `messages`.`convo`=$id GROUP BY `headers`.`value` ORDER BY COUNT(`headers`.`value`) DESC"));
        $row2 = $db->get_first("SELECT COUNT(id) FROM `messages` WHERE `convo`='$id'");
        $db->query("UPDATE `convos` SET `summary`='$summary', `participants`='$participants', `updated`=NOW(), `messages`=".$row2["COUNT(id)"]." WHERE `id`=$id");
    }
    
    public function create_id() {
        global $db;
        $this->id = $db->query("INSERT INTO `messages` (`created`) VALUES (NOW())", true);
    }
    public function add_raw($head, $body) {
        global $db;
        $this->head = $db->escape($head);
        $this->body = $db->escape($body);
    }
    
    public function parse_head($head) {
        $lines = split("\r?\n", $head);
        $header = array();
        foreach ($lines as $line) {
            $this->parse_head_line($line);
        }
    }
    
    public function parse_head_line($line) {
        global $header;
        if (substr($line, 0, 1) == "\t" || substr($line, 0, 1) == " ") {
            $header[1] .= substr($line, 1);
        }
        else {
            if (sizeof($header)) {
                $this->add_header($header[0], $header[1]);
                $header = array();
            }
            if ($line == "\n") {
                $header = FALSE;
            }
            else {
                $header = split(":", $line);
            }
        }
    }
    
    public function add_header($name, $value) {
        $this->headers[] = array($name, $value);
    }
    
    public function real_add_header($name, $value) {
        global $db;
        $name = $db->escape($name);
        $value = $db->escape($value);
        $db->query("INSERT INTO `headers` (`message`, `name`, `value`, `user`) VALUES ('$this->id', '$name', '$value', '$this->user')");
    }
}

?>