<?php

require_once("db.php");

function get_setting($name) {
    if ($name == "name") return "John Smith";
    elseif ($name == "html") return "false";
}

function contents() {
    global $theme;
    global $user;
    global $db;
    global $username;
    
    $do = $_GET['do'];
    
    if ($do == "new") {
        
        echo "<h2>New Email</h2>";
        include("themes/$theme/compose.php");
      
    }
    elseif ($do == "send") {
        
        include("send.php");
        
    }
    elseif ($do == "convo") {
        
        $convo = preg_replace("[^0-9]", "", $_GET['convo']);
        
        $convo = new Convo($user, $convo);
        $messages = $convo->getMessages();
        
        include("themes/$theme/view.php");
        
    }
    else {
    
        $title = "Inbox";
        $selecttools = "Select: <a href=\"javascript:selall()\">All</a>, <a href=\"javascript:selnone()\">None</a>, <a href=\"javascript:selread()\">Read</a>, <a href=\"javascript:selunread()\">Unread</a>";
        $convos = array();
        $navi = ($liststart+1)." - $listend of $total<br/>";
        $view = "inbox";
        $convos = array();
        $r = $db->query("SELECT * FROM `convos` WHERE user=$user ORDER BY `updated` DESC");
        while ($row = $db->iter($r)) {
            $convos[] = $row;
        }
        
        $actions = "";
        if ($view == "inbox")
                $actions .= "<button type=\"button\" onclick=\"javascript:moreact('arc')\">Archive</button>";
        elseif ($view == "arc")
                $actions .= "<button type=\"button\" onclick=\"javascript:moreact('unarc')\">Move to Inbox</button>";
        elseif ($view == "tag")
                $actions .= "<button type=\"button\" onclick=\"javascript:moreacts('untag','".$tag."')\">Remove tag</button>";
        if ($view == "bin")
                $actions .= " <button type=\"button\" onclick=\"javascript:moreact('realdel')\">Delete Forever</button> <button type=\"button\" onclick=\"javascript:moreact('undel')\">Restore</button>";
        else
                $actions .= " <button type=\"button\" onclick=\"javascript:moreact('del')\">Delete</button>";
        $actions .= " <select><option>More Actions</option>";
        if ($view == "arc") $actions .= "<option onclick=\"javascript:moreact('arc')\">&nbsp;&nbsp;Archive</option>";
        elseif ($view!="inbox" && $view!="bin") $actions .= "<option onclick=\"javascript:moreact('unarc')\">&nbsp;&nbsp;Move to Inbox</option><option onclick=\"javascript:moreact('arc')\">&nbsp;&nbsp;Archive</option>";
        $actions .= "<option onclick=\"javascript:moreact('read')\">&nbsp;&nbsp;Mark as Read</option><option onclick=\"javascript:moreact('unread')\">&nbsp;&nbsp;Mark as Unread</option><option onclick=\"javascript:moreact('star')\">&nbsp;&nbsp;Add star</option><option onclick=\"javascript:moreact('unstar')\">&nbsp;&nbsp;Remove star</option>";
        $actions .= "<option>Add Tag</option><option onclick=\"moreacts('newtag','')\">&nbsp;&nbsp;New Tag...</option>";
        global $con;
        global $db_prefix;
        global $user;
        if (isset($tags)) {
           foreach ($tags as $tag) {
                $actions .= "<option onclick=\"moreacts('tag','".$row["name"]."')\">&nbsp;&nbsp;".$row["name"]."</option>";
        }
        }
        $actions .= "</select> <a href=\"$self\">Refresh</a>";
        
        include("themes/$theme/list.php");
        
    }
}

$theme = "default";
$username = "test@xn--gce.com";
$user = 1;

include("themes/$theme/header.php");

#if (!$_SESSION["user"]) {
if ($username) {
    include("themes/$theme/main.php");
}
else {
    include("themes/$theme/login.php");
}

include("themes/$theme/footer.php");

?>