<?php
    $message = new Message($username);
    $message->create_id();
    
    if (get_magic_quotes_gpc()) {
            $_POST = array_map('stripslashes', $_POST);
    }
    $part["type"] = TYPETEXT;
    if ($_POST["html"] == "true") {
            $part["subtype"] = "HTML";
            $html = 1;
    } else {
            $part["subtype"] = "PLAIN";
            $html = 0;
    }
    $part["description"] = "test";
    $part["contents.data"] = $_POST["content"];
    $envelope["message_id"] =  "<".$message->id."@".preg_replace("/^.*?\@/", "", $username).">";
    $envelope["from"] = get_setting("name")." <$username>";
    $envelope["cc"] = $_POST["cc"];
    $envelope["bcc"] = $_POST["bcc"];
    if ($_SESSION["in_reply_to"]) $envelope["in_reply_to"] = $_SESSION["in_reply_to"];
    #$envelope["to"] = $_POST["to"];
    #$envelope["subject"] = $_POST["subject"];
    $comp = imap_mail_compose($envelope, array($part));
    list($t_header,$t_body)=split("\r\n\r\n",$comp,2);
    
    $message->type = $message->TYPE_SENT;
    $message->add_raw($t_header, $t_body);
    $message->add_header("To", $_POST["to"]);       
    $message->add_header("Subject", $_POST["subject"]);
    $message->parse_head($t_header);
    if ($_GET['convo']) {
        $convo = $_GET['convo'];
        $message->finish($convo);
    }
    else {
        $message->finish();
    }
    
    imap_mail($_POST["to"], $_POST["subject"], $t_body, $t_header);
    
?>
Message sent.