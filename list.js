function hili(num,base) {
	if (document.getElementById("tick"+num).checked == true) {
		document.getElementById("mess"+num).className = base+"_sel";
	}
	else {
		document.getElementById("mess"+num).className = base;
	}
}
function tick(chk,val) {
	if (chk) {
		if (chk.length) {
			for (i = 0; i < chk.length; i++) {
				chk[i].checked = val;
				chk[i].onchange();
			}
		}
		else {
			chk.checked = val;
			chk.onchange();
		}
	}
}
function selall() {
	tick(document.getElementById("listform").check_read,true);
	tick(document.getElementById("listform").check_unread,true);
}
function selnone() {
	tick(document.getElementById("listform").check_read,false);
	tick(document.getElementById("listform").check_unread,false);
}
function selread() {
	tick(document.getElementById("listform").check_read,true);
	tick(document.getElementById("listform").check_unread,false);
}
function selunread() {
	tick(document.getElementById("listform").check_unread,true);
	tick(document.getElementById("listform").check_read,false);
}
function moreact(value) {
	moreacts(value,"");
}
function moreacts(value,tagname) {
	range="";
	first=true;
	for (i in convoarr) {
		if (document.getElementById("tick"+convoarr[i])) {
			if (document.getElementById("tick"+convoarr[i]).checked) {
				if (first) {
					first = false;
				}
				else {
					range += ",";
				}
				range += convoarr[i];
			}
		}
		i++;
	}
	if (range == "") {
		alert("Please select one or more messages.");
	}
	else {
		do_actions(value, tagname, range);
	}
}

