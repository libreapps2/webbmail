<?php

require_once("config.php");

class MySQLDataBase {
    private $connection;
    public $verbose = false;
    
    public function __construct() {
        global $config;
        $this->connection = mysql_connect($config->db_server, $config->db_user, $config->db_password);
        mysql_select_db($config->db_db, $this->connection);
    }
    
    public function escape($string) {
        return mysql_real_escape_string($string);
    }
    
    public function query($sql, $returnid=false) {
        if ($this->verbose) echo $sql."\n";
        if ($returnid) {
            if (mysql_query($sql, $this->connection)) {
                return mysql_insert_id();
            }
            else {
                throw new Exception("MySQL query failed.");
            }
        }
        else {
            echo mysql_error();
            return mysql_query($sql, $this->connection);
        }
    }
    
    public function iter($result) {
        return mysql_fetch_assoc($result);
    }
    
    public function get_first($sql) {
        if ($r = $this->query($sql)) {
            return $this->iter($r);
        }
        else return false;
    }
    
    public function get_list($sql) {
        if ($r = $this->query($sql)) {
            $out = "";
            while ($row = $this->iter($r)) {
                if ($out) $out .= ", ";
                $out .= trim($row["value"]);
            }
            return $out;
        }
        else return false;
    }
}

$db = new MySQLDataBase();

require_once("message.php");
require_once("convo.php");

?>