SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS `convos` (
  `id` int(11) NOT NULL auto_increment,
  `summary` varchar(100) NOT NULL,
  `participants` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `read` tinyint(1) NOT NULL,
  `user` int(11) NOT NULL,
  `messages` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

CREATE TABLE IF NOT EXISTS `headers` (
  `id` int(11) NOT NULL auto_increment,
  `message` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=728 ;

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL auto_increment,
  `convo` int(11) NOT NULL,
  `body` text NOT NULL,
  `user` int(11) NOT NULL,
  `head` text NOT NULL,
  `type` int(11) NOT NULL default '0',
  `created` datetime NOT NULL,
  `read` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

