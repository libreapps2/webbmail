<?php
require_once("db.php");
#$db->verbose = true;

$fp = fopen("php://stdin", "r");
$header = true;
$head = "";
$body = "";
$message = new Message($argv[1]);
$header = array();
while ($line = fgets($fp)) {
    if ($header === FALSE) {
        $body .= $line;
    }
    else {
        $head .= $line;
        $message->parse_head_line($line);
    }
}
$message->add_raw($head, $body);
$message->finish();
fclose($fp);

?>
